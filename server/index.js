require('dotenv').config()
const express = require('express');

require('./models/associations');

const indexRouter = require('./api/index');
const usersRouter = require('./api/users');
const Socket = require('./Socket');


const app = express();
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use((req, res, next) => res.status(404).send('API not found'));

app.use((err, req, res, next) => {
    console.error(err);
    res.status(err.status || 500).send('Internal Server Error');
});

const port = process.env.PORT || 3000;

/**
 * @type {http.Server}
 */
let server = app.listen(port, () => console.log(`Chibi-Infra listening on port ${port}!`));
Socket.init(server);
