const {Score: ScoreModel} = require('../models/Score');

module.exports = class Score {
    static async create(value, user) {
        await ScoreModel.create({value, user});
    }

    static async top10() {
        return await ScoreModel.findAll({limit: 10, order: [['value', 'DESC']]});
    }
};