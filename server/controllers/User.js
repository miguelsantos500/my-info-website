const {User: UserModel} = require('../models/User');

module.exports = class User {
    static async create(key, name, admin = false) {
        return await UserModel.create({key, name, admin});
    }

    static async list() {
        return await UserModel.findAll({});
    }
    static async findOne(filter = {}) {
        return await UserModel.findOne(filter);
    }
    static async findByUserId(userId) {
        return await User.findOne({key:userId});
    }
};
