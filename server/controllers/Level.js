const {Level: LevelModel} = require('../models/Level');

module.exports = class Level {
    static async list() {
        await LevelModel.findAll();
    }
};