const {Message: MessageModel} = require('../models/Message');
const {User: UserModel} = require('../models/User');
const User = require('../controllers/User');

module.exports = class Message {
    static async create(message, username, userId) {
        let user = await User.findOne({where: {key: userId}});
        if (user === null) {
            user = await User.create(userId, username);
        }
        return await MessageModel.create({message, userId: user.id});

    }

    static async latest() {
        return await MessageModel.findAll({
            limit: 10, order: [['createdAt', 'ASC']], include: [{
                model: UserModel
            }]
        });
    }
};
