const Sequelize = require('sequelize');

const DATABASE_URI = process.env.DATABASE_URI || 'postgres://chibi:123qwerty@localhost:5432/infra';

const sequelize = new Sequelize(DATABASE_URI);
module.exports = {Sequelize, sequelize};