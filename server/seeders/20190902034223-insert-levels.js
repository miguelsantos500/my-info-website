const ENUM = require('../utils/enum');
const {BRICK, NEWLINE} = ENUM.LEVEL.ITEM.TYPE;


module.exports = {
    up: (queryInterface, Sequelize) => queryInterface.bulkInsert('Levels', [{
        index: 1,
        content: JSON.stringify([
            {
                type: BRICK,
                qty: 10,
            },
            {
                type: NEWLINE,
                qty: 1,
            },
            {
                type: BRICK,
                qty: 9,
            },
            {
                type: NEWLINE,
                qty: 1,
            },
            {
                type: BRICK,
                qty: 10,
            },
            {
                type: NEWLINE,
                qty: 1,
            },
            {
                type: BRICK,
                qty: 9,
            }
        ]),
        createdAt: new Date(),
        updatedAt: new Date()
    }, {
        index: 2,
        content: JSON.stringify([
            {
                type: BRICK,
                qty: 9,
            },
            {
                type: NEWLINE,
                qty: 1,
            },
            {
                type: BRICK,
                qty: 8,
            },
            {
                type: NEWLINE,
                qty: 1,
            },
            {
                type: BRICK,
                qty: 9,
            }
        ]),
        createdAt: new Date(),
        updatedAt: new Date()
    }], {}),
    down: (queryInterface, Sequelize) => queryInterface.bulkDelete('Levels', null, {})
};
