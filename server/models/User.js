const {Sequelize, sequelize} = require('../utils/clients/database');

class User extends Sequelize.Model {
}
User.init({
  key: Sequelize.STRING,
  name: Sequelize.STRING,
  admin: Sequelize.BOOLEAN,
}, {sequelize});

module.exports = {User};

