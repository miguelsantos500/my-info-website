const {Message} = require('./Message');
const {User} = require('./User');

Message.belongsTo(User, {foreignKey: 'userId'});
