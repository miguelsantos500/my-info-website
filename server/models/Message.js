const {Sequelize, sequelize} = require('../utils/clients/database');

class Message extends Sequelize.Model {
}
Message.init({
  message: Sequelize.STRING
}, {sequelize});

module.exports = {Message};

