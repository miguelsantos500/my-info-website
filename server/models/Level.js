const {Sequelize, sequelize} = require('../utils/clients/database');

class Level extends Sequelize.Model {
}
Level.init({
  index: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  content: {
    type: Sequelize.JSON,
    allowNull: false
  }
}, {sequelize});

module.exports = {Level};

