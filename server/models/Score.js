const {Sequelize, sequelize} = require('../utils/clients/database');

class Score extends Sequelize.Model {
}
Score.init({
  value: Sequelize.INTEGER,
  user: Sequelize.STRING
}, {sequelize});

module.exports = {Score};

