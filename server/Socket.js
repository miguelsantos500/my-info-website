const Level = require('./controllers/Level');
const Score = require('./controllers/Score');
const Message = require('./controllers/Message');
const User = require('./controllers/User');

class Server {
    /**
     * @type {http.Server}
     */
    static init(server) {
        const io = require('socket.io')(server);

        io.on('connection', (socket) => {
            console.log('User Connected!');
            socket.on('levels:list?', async () => {
                let levels = await Level.list();
                socket.emit('levels:list!', levels);
            });

            socket.on('score:publish?', async (data) => {
                const {value, user} = data;
                await Score.create(value, user);
                let scores = await Score.top10();
                io.sockets.emit('scores:top10!', {scores});
            });
            socket.on('scores:top10?', async () => {
                let scores = await Score.top10();
                socket.emit('scores:top10!', {scores});
            });
            socket.on('messages:latest?', async (data) => {
                const userId = data.userId;
                const messages = await Message.latest();
                const messagesToSend = messages.map((m) => {
                    const data = Object.assign({}, m.dataValues);
                    data.isYou = data.User.key === userId;
                    data.username = data.User.name;
                    data.admin = data.User.admin;
                    delete data.User;
                    return data;
                });
                socket.emit('messages:latest!', {messages: messagesToSend});
            });
            socket.on('message:publish?', async (data) => {
                const message = await Message.create(data.message, data.username, data.userId);
                const user = await User.findByUserId(data.userId);
                //This means that a message sent by the user will show as not him in another tab...
                //Have to find a way to make isYou: dynamic without passing userId. Rooms?
                socket.broadcast.emit('message:new!', Object.assign(message.dataValues, {
                    isYou: false,
                    username: user.name,
                    admin: user.admin
                }));
            });

        });
    }
}

module.exports = Server;
