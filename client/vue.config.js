module.exports = {
    chainWebpack: config => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader')
            .tap(options => {
                // modify the options...
                options.transformAssetUrls = {
                    video: ['src', 'poster'],
                    source: 'src',
                    img: 'src',
                    image: 'xlink:href',
                    'b-img': 'src',
                    'b-img-lazy': ['src', 'blank-src'],
                    'b-card': 'img-src',
                    'b-card-img': 'src',
                    'b-card-img-lazy': ['src', 'blank-src'],
                    'b-carousel-slide': 'img-src',
                    'b-embed': 'src'
                };
                return options;
            });

        /* disable insertion of assets as data urls b/c Phaser doesn't support it */
        const rules = [
            {name: 'images', dir: 'img'},
            {name: 'media', dir: 'media'}
        ];
        rules.forEach((rule) => {
            const ruleConf = config.module.rule(rule.name);
            ruleConf.uses.clear();
            ruleConf
                .use('file-loader')
                .loader('file-loader')
                .options({
                    name: `${rule.dir}/[name].[hash:8].[ext]`
                })
        })
    },
    devServer: {
        open: true,
        hot: false
    },
    //Force PWA reload when website is updated
    pwa: {
        workboxOptions: {
            skipWaiting: true,
            clientsClaim: true
        }
    }
};
