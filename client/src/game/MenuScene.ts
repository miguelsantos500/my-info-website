import 'phaser';

import ENUM from './Enum';

export class MenuScene extends Phaser.Scene {
    private btnPlay!: Phaser.GameObjects.Sprite;

    constructor() {
        super({
            key: 'MenuScene',
        });
    }

    public preload(): void {
        this.load.image('sprBtnPlay', require('@/assets/images/breakout/new_game.png'));
        this.load.image('sprBtnPlayDown', require('@/assets/images/breakout/new_game_down.png'));
        this.load.image('sprBtnPlayHover', require('@/assets/images/breakout/new_game_down.png'));
    }

    public create(): void {
        this.btnPlay = this.add.sprite(ENUM.GAME.CENTER.WIDTH, ENUM.GAME.CENTER.LENGTH, 'sprBtnPlay');
        this.btnPlay.scale = 0.5;
        this.btnPlay.setInteractive();
        this.btnPlay.on('pointerover', () => this.btnPlay.setTexture('sprBtnPlayHover'));
        this.btnPlay.on('pointerdown', () => this.btnPlay.setTexture('sprBtnPlayDown'));
        this.btnPlay.on('pointerout', () => this.btnPlay.setTexture('sprBtnPlay'));
        this.btnPlay.on('pointerup', () => {
            this.btnPlay.setTexture('sprBtnPlay');
            this.scene.start('BreakoutScene');
        });
    }

    public update(time: any): void {
        // Empty
    }
}
