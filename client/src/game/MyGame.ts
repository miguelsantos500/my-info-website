import 'phaser';
// import {GameScene} from './GameScene';
import {BreakoutScene} from './BreakoutScene';
import {GameOverScene} from './GameOverScene';
import {MenuScene} from './MenuScene';
import ENUM from './Enum';

export class MyGame {
    public game: Phaser.Game;

    constructor() {
        this.game = new Phaser.Game({
            type: Phaser.AUTO,
            width: ENUM.GAME.WIDTH,
            height: ENUM.GAME.LENGTH,
            parent: 'game-container',
            physics: {
                default: 'arcade',
                arcade: {
                    gravity: {y: 0},
                    debug: false,
                },
            },
            scene: [MenuScene, BreakoutScene, GameOverScene],
        });
    }
}

/*

let loader: Promise<typeof MyGame> = new Promise(async $export => {
    const module = await Promise.resolve(MyGame);
    $export(module);
});

export {loader}*/
