const ENUM = {
    GAME: {
        WIDTH: 800,
        LENGTH: 600,
        CENTER: {
            WIDTH: 0,
            LENGTH: 0,
        },
    },
};
ENUM.GAME.CENTER.WIDTH = ENUM.GAME.WIDTH / 2;
ENUM.GAME.CENTER.LENGTH = ENUM.GAME.LENGTH / 2;

export default ENUM;
