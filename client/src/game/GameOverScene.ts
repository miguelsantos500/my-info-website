import 'phaser';

import ENUM from './Enum';
import {Socket} from '../Socket';

export class GameOverScene extends Phaser.Scene {

    private btnPlay!: Phaser.GameObjects.Sprite;

    constructor() {
        super({
            key: 'GameOverScene',
        });
    }

    public preload(): void {
        this.load.image('sprBtnPlay', require('@/assets/images/breakout/new_game.png'));
        this.load.image('sprBtnPlayDown', require('@/assets/images/breakout/new_game_down.png'));
        this.load.image('sprBtnPlayHover', require('@/assets/images/breakout/new_game_down.png'));
    }

    public create(object: { score: number, restart: string }): void {
        this.add.rectangle(ENUM.GAME.CENTER.WIDTH, ENUM.GAME.CENTER.LENGTH, ENUM.GAME.WIDTH, ENUM.GAME.LENGTH,
            0x3c3c3c);
        this.add.text(ENUM.GAME.CENTER.WIDTH, ENUM.GAME.CENTER.LENGTH, 'Game Over... Score: ' + object.score);
        this.btnPlay = this.add.sprite(ENUM.GAME.CENTER.WIDTH + 150, ENUM.GAME.CENTER.LENGTH + 50, 'sprBtnPlay');
        this.btnPlay.scale = 0.5;
        this.btnPlay.setInteractive();
        this.btnPlay.on('pointerover', () => this.btnPlay.setTexture('sprBtnPlayHover'));
        this.btnPlay.on('pointerdown', () => this.btnPlay.setTexture('sprBtnPlayDown'));
        this.btnPlay.on('pointerout', () => this.btnPlay.setTexture('sprBtnPlay'));
        this.btnPlay.on('pointerup', () => {
            this.btnPlay.setTexture('sprBtnPlay');
            this.scene.start(object.restart);
        });
        this.game.events.emit('publish-score', {score: object.score});
    }

    public update(time: any): void {
        // Empty
    }

}
