import 'phaser';

import ENUM from './Enum';

const OBJECT = {
    PLAYER: 'player',
    BALL: 'ball',
    BRICK: 'brick',
    BOTTOM: 'bottom',
};
const Y_VELOCITY_DEFICIT = 0.7;

export class BreakoutScene extends Phaser.Scene {

    private cursors!: Phaser.Types.Input.Keyboard.CursorKeys;
    private gameOver: boolean;
    private player!: Phaser.Physics.Arcade.Image;
    private bricks!: Phaser.Physics.Arcade.Group;
    private ball!: Phaser.Physics.Arcade.Image;
    private bottom!: Phaser.Physics.Arcade.Group;
    private scoreText!: Phaser.GameObjects.Text;

    private playerSpeed: number;
    private ballSpeed: number;
    private temp = {
        ball: {
            x: -1, y: -1,
        },
    };
    private score = 0;

    constructor() {
        super({
            key: 'BreakoutScene',
        });
        this.gameOver = false;
        this.playerSpeed = 300;
        this.ballSpeed = 400;
    }

    public preload(): void {
        this.load.image(OBJECT.PLAYER, require('@/assets/images/breakout/player.png'));
        this.load.image(OBJECT.BALL, require('@/assets/images/breakout/ball.png'));
        const brick = this.add.graphics({
            x: 0, y: 0, lineStyle: {
                width: 1,
                color: 0xffffff,
                alpha: 1,
            },
            fillStyle: {
                color: 0xffffff,
                alpha: 1,
            },
        });
        brick.fillRect(5, 5, 60, 30);
        brick.generateTexture(OBJECT.BRICK, 60, 30);
        const bottom = this.add.graphics({
            x: 0, y: 0, lineStyle: {
                width: 1,
                color: 0x000000,
                alpha: 1,
            },
            fillStyle: {
                color: 0x000000,
                alpha: 1,
            },
        });
        bottom.fillRect(5, 5, ENUM.GAME.WIDTH, 5);
        bottom.generateTexture(OBJECT.BOTTOM, ENUM.GAME.WIDTH, 5);
    }

    public create(): void {
        this.gameOver = false;
        this.score = 0;
        this.add.rectangle(ENUM.GAME.CENTER.WIDTH, ENUM.GAME.CENTER.LENGTH, ENUM.GAME.WIDTH, ENUM.GAME.LENGTH,
            0x3c3c3c);

        this.ball = this.physics.add.image(ENUM.GAME.CENTER.WIDTH, ENUM.GAME.CENTER.LENGTH, OBJECT.BALL);
        this.ball.scale = 0.4;
        const ballVelocityX = (Math.random() * 401) - 200;
        this.ball.setVelocity(Math.round(ballVelocityX),
            (this.ballSpeed - Math.abs(ballVelocityX) * Y_VELOCITY_DEFICIT));
        this.ball.setCollideWorldBounds(true, 1, 1);

        this.player = this.physics.add.image(ENUM.GAME.CENTER.WIDTH, ENUM.GAME.LENGTH * 0.9, OBJECT.PLAYER);
        this.player.setCollideWorldBounds(true);

        this.physics.add.collider(this.player, this.ball, this.bounceBallOnPlayer, undefined, this);
        this.bricks = this.physics.add.group({
            key: OBJECT.BRICK,
            repeat: 10,
            setXY: {x: 60, y: 60, stepX: 65},
        });
        this.bricks.createFromConfig({
            key: OBJECT.BRICK,
            repeat: 9,
            setXY: {x: 90, y: 95, stepX: 65},
        });
        this.bricks.createFromConfig({
            key: OBJECT.BRICK,
            repeat: 10,
            setXY: {x: 60, y: 130, stepX: 65},
        });
        this.bricks.createFromConfig({
            key: OBJECT.BRICK,
            repeat: 9,
            setXY: {x: 90, y: 165, stepX: 65},
        });
        this.physics.add.overlap(this.ball, this.bricks, this.bounceBallOnBrick, undefined, this);

        // TODO: Remove group and greate single
        this.bottom = this.physics.add.group({
            key: OBJECT.BOTTOM,
            repeat: 1,
            setXY: {x: ENUM.GAME.CENTER.WIDTH, y: ENUM.GAME.LENGTH},
        });
        this.physics.add.collider(this.ball, this.bottom, this.lose, undefined, this);

        this.scoreText = this.add.text(16, 16, 'Score: 0', {fontSize: '16px'});

        this.cursors = this.input.keyboard.createCursorKeys();
    }

    public update(time: any): void {
        if (this.cursors.left!.isDown) {
            this.player.setVelocityX(this.playerSpeed * -1);
        } else if (this.cursors.right!.isDown) {
            this.player.setVelocityX(this.playerSpeed);
        } else {
            this.player.setVelocityX(0);
        }
        this.player.setY(ENUM.GAME.LENGTH * 0.9);
    }

    private bounceBallOnBrick(object1: Phaser.GameObjects.GameObject, object2: Phaser.GameObjects.GameObject) {
        const ball = object1 as Phaser.Physics.Arcade.Image;
        const brick = object2 as Phaser.Physics.Arcade.Image;

        if (this.temp.ball.x === ball.body.x && this.temp.ball.y === ball.body.y) {
            return;
        }
        this.temp.ball.x = ball.body.x;
        this.temp.ball.y = ball.body.y;

        if (brick.body.touching.up || brick.body.touching.down) {
            ball.setVelocity(ball.body.velocity.x, ball.body.velocity.y * -1);
        } else {
            ball.setVelocity(ball.body.velocity.x * -1, ball.body.velocity.y);
        }
        brick.disableBody(true, true);
        this.score += 1;
        this.scoreText.setText('Score: ' + this.score);
    }

    private bounceBallOnPlayer(object1: Phaser.GameObjects.GameObject, object2: Phaser.GameObjects.GameObject) {
        const ball = object2 as Phaser.Physics.Arcade.Image;
        const player = object1 as Phaser.Physics.Arcade.Image;

        if (ball.body.touching.left || ball.body.touching.right) {
            ball.setBounce(1, 1);
        } else {
            let x = Math.round(ball.body.x - player.getTopCenter().x) * 1.8;
            if (x >= this.ballSpeed - 50) {
                x = this.ballSpeed - 50;
            }
            const y = (this.ballSpeed - Math.abs(x)) * -1;
            ball.setVelocity(x, y);
            player.setVelocity(0, 0);
        }
    }

    private lose(object1: Phaser.GameObjects.GameObject, object2: Phaser.GameObjects.GameObject) {
        if (this.gameOver === true) {
            return;
        }
        this.gameOver = true;
        this.scene.start('GameOverScene', {score: this.score, restart: 'BreakoutScene'});
    }
}
