import io from 'socket.io-client';

export class Socket {

    public static socket: SocketIOClient.Socket;

    public static init() {
        Socket.socket = io(process.env.VUE_APP_SERVER_URL || 'https://miguel-santos.ddns.net/');
    }

    public static publishScore(score: number, user: string) {
        Socket.socket.emit('score:publish?', {value: score, user});
    }

    public static sendMessage(username: string, message: string, userId: string) {
        Socket.socket.emit('message:publish?', {username, message, userId});
    }

    public static getLatestMessages(userId: string) {
        Socket.socket.emit('messages:latest?', {userId});
    }

}
