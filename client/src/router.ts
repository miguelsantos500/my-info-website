import Vue from 'vue';
import Router from 'vue-router';
import Index from './views/Index.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index,
        },
        /*{
            path: '/team',
            name: 'team',
            // route level code-splitting
            // this generates a separate chunk (team.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import( './views/Team.vue'),
        },*/
        {
            path: '/website',
            name: 'website',
            // route level code-splitting
            // this generates a separate chunk (team.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "website" */ './views/Website.vue'),
        },
        {
            path: '/game',
            name: 'game',
            // route level code-splitting
            // this generates a separate chunk (game.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "game" */ './views/Game.vue'),
        },
    ],
});
