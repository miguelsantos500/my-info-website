export class Message {
    private _message: string;
    private readonly _isYou: boolean | null;
    private readonly _admin: boolean;
    private readonly _username: string;
    private readonly _date: Date;

    constructor(message: string, date: Date, username: string, isYou: boolean | null, admin: boolean) {
        this._message = message;
        this._isYou = isYou;
        this._admin = admin;
        this._date = date;
        this._username = username;
    }

    get message(): string {
        return this._message;
    }

    set message(value: string) {
        this._message = value;
    }

    get isYou(): boolean | null {
        return this._isYou;
    }

    get username(): string {
        return this._username;
    }

    get date(): Date {
        return this._date;
    }

    get admin(): boolean {
        return this._admin;
    }
}
