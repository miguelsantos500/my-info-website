export class Skill {
    private readonly _name: string;
    private readonly _score: number;

    constructor(name: string, score: number) {
        this._name = name;
        this._score = score;
    }

    get name(): string {
        return this._name;
    }

    get score(): number {
        return this._score;
    }
}
