import Vue from 'vue';

export default {
    technologies: [
        {
            name: 'Vue',
            link: 'https://vuejs.org',
            imagePath: 'technologies/vue.png',
            description: 'technology-vue',
        },
        {
            name: 'Bootstrap',
            link: 'https://getbootstrap.com/',
            imagePath: 'technologies/bootstrap.png',
            description: 'technology-bootstrap',
        },
        {
            name: 'Node.js',
            link: 'https://nodejs.org/',
            imagePath: 'technologies/nodejs.png',
            description: 'technology-nodejs',
        },
        {
            name: 'Docker Swarm',
            link: 'https://docs.docker.com/swarm/overview/',
            imagePath: 'technologies/docker-swarm.png',
            description: 'technology-docker-swarm',
        },
        {
            name: 'Traefik',
            link: 'https://containo.us/traefik/',
            imagePath: 'technologies/traefik.png',
            description: 'technology-traefik',
        },
        {
            name: 'PostgreSQL',
            link: 'https://www.postgresql.org/',
            imagePath: 'technologies/postgres.png',
            description: 'technology-postgresql',
        },
    ],
    skills: {
        languages:
            [
                {name: 'Javascript', score: 90},
                {name: 'Java', score: 90},
                {name: 'Ruby', score: 80},
                {name: 'Typescript', score: 80},
                {name: 'C#', score: 70},
                {name: 'PHP', score: 70},
                {name: 'C', score: 60},
                {name: 'Bash', score: 60},
            ],
        others: [
            {name: 'Node.js', score: 100},
            {name: 'Jira', score: 90},
            {name: 'Docker', score: 90},
            {name: 'MongoDB', score: 90},
            {name: 'OracleSQL', score: 80},
            {name: 'Vue.js', score: 80},
            {name: 'JQuery', score: 80},
            {name: 'Android', score: 80},
            {name: 'Angular', score: 70},
            {name: 'PostgreSQL', score: 70},
            {name: 'MySql', score: 70},
            {name: '.Net Core', score: 70},
        ],
    },
};

