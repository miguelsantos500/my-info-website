export class Technology {
    private readonly _name: string;
    private readonly _link: string;
    private readonly _imagePath: string;
    private readonly _description: string;

    constructor(name: string, link: string, imagePath: string, description: string) {
        this._name = name;
        this._link = link;
        this._imagePath = imagePath;
        this._description = description;
    }

    get name(): string {
        return this._name;
    }

    get link(): string {
        return this._link;
    }

    get imagePath(): string {
        return this._imagePath;
    }

    get description(): string {
        return this._description;
    }
}
