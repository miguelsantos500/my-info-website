import Vue from 'vue';
// @ts-ignore
import {MLInstaller, MLCreate, MLanguage} from 'vue-multilanguage';

Vue.use(MLInstaller);

export default new MLCreate({
    initial: 'english',
    save: process.env.NODE_ENV === 'production',
    languages: [
        new MLanguage('english').create({
            'app-lang': 'Language',
            'english': 'EN',
            'portuguese': 'PT',
            'h1-team': 'Team',
            'website-developed': 'This website was Developed and Deployed by me.',
            'website-my-technologies': 'Here are the technologies I used:',
            'technology-vue': 'is an open-source progressive framework for building user interfaces.',
            'technology-bootstrap': 'is an open source toolkit for developing with HTML, CSS, and JS.',
            'technology-nodejs': 'is an open-source, cross-platform, JavaScript runtime environment that executes JavaScript code outside of a browser.',
            'technology-docker-swarm': 'is an open-source container orchestration platform and is the native clustering engine for and by Docker.',
            'technology-traefik': 'is an open source reverse proxy and load balancer for HTTP and TCP-based applications.',
            'technology-postgresql': 'is an open-source relational database management system emphasizing extensibility and technical standards compliance.',
            'my-username': 'My username is:',
            'open-chat': 'Open chat',
            'type-message': 'Type a message',
            'send-message': 'Send',
            'contact-me': 'Contact me at miguelsantos500@gmail.com',
        }),
        new MLanguage('portuguese').create({
            'app-lang': 'Idioma',
            'english': 'EN',
            'portuguese': 'PT',
            'h1-team': 'Equipa',
            'website-developed': 'Este site foi desenvolvido e colocado em Produção por mim.',
            'website-my-technologies': 'Aqui estão as tecnologias que utilizei:',
            'technology-vue': 'é uma framework open-source para desenvolver interfaces de utilizador.',
            'technology-bootstrap': 'é uma ferramenta open-source para desenvolver com HTML, CSS, and JS.',
            'technology-nodejs': 'é um ambiente de execução em Javascript, open-source e multi-plataforma que executa código JavaScript sem ser num browser.',
            'technology-docker-swarm': 'é um orquestrador de containers open-source e é o motor de clustering para e desenvolvido por Docker.',
            'technology-traefik': 'é um load-balancer e reverse-proxy para aplicações HTTP e TCP.',
            'technology-postgresql': 'é uma base de dados relacional e open-source que realça extensibilidade e conformidade com padrões técnicos.',
            'my-username': 'O meu username é:',
            'open-chat': 'Abrir chat',
            'type-message': 'Escreva uma mensagem',
            'send-message': 'Enviar',
            'contact-me': 'Contacte-me via miguelsantos500@gmail.com',
        }),
    ],
});
